const fs = require('fs');
require('dotenv').config()
require('log-timestamp');
const axios = require('axios').default;

const baseTXT = process.env.FILES_PATH + process.env.TXT_FILENAME;
console.log(`Watching for file changes on ${baseTXT}`);

const baseALL = process.env.FILES_PATH + process.env.ALL_FILENAME;
console.log(`Watching for file changes on ${baseALL}`);

fs.watchFile(baseTXT, (curr, prev) => {
    console.log(`${baseTXT} file Changed`);
});

fs.watchFile(baseALL, (curr, prev) => {
    console.log(`${baseALL} file Changed`);
});