# Base Update

O projeto Base Update foi criado para a substituição da CRON de upload de base dos clientes EBC e Ikesaki, podendo ser estendido para os demais futuros clientes com o procedimento similar de upload de base de clientes.

## Instalação

Após o download do projeto via GIT, siga os procedimentos abaixo.

```bash
npm install
cp .env.example .env
vim .env
```

E configure as variáveis de ambiente no arquivo.

## Uso
Na pasta do projeto, digite o seguinte comando:
```bash
pm2 start index.js
```

## O sistema de versionamento do projeto é baseado em git flow.

## Licensa
[ISC](https://choosealicense.com/licenses/isc/)

## Autor
Rodrigo R. Almeida @ Advance Telecom.

_<Code is Poetry>_